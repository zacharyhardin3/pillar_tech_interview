import unittest
import nose.tools as nt
from yahtzee import Yahtzee

class YahtzeeTest(unittest.TestCase):
    def setUp(self):
        self.yahtzee = Yahtzee()

    def test_program_returns_int(self):
        result = self.yahtzee.calculate_yahtzee_points([], 0)
        nt.assert_equal(type(result), int)

    def test_should_return_8_when_two_4(self):
        dice_roll = [1, 1, 2, 4, 4]
        category = 4

        result = self.yahtzee.calculate_yahtzee_points(dice_roll, category)
        nt.assert_equal(result, 8)

    def test_should_return_18_when_three_6(self):
        dice_roll = [6, 6, 6, 0, 0]
        category = 6

        result = self.yahtzee.calculate_yahtzee_points(dice_roll, category)
        nt.assert_equal(result, 18)

    def test_should_return_8_when_pair_of_4(self):
        dice_roll = [3, 3, 3, 4, 4]
        category = "Pair"

        result = self.yahtzee.calculate_yahtzee_points(dice_roll, category)
        nt.assert_equal(result, 8)

    def test_should_return_6_when_pair_of_3(self):
        dice_roll = [1, 3, 3, 2, 2]
        category = "Pair"

        result = self.yahtzee.calculate_yahtzee_points(dice_roll, category)
        nt.assert_equal(result, 6)

    def test_should_return_zero_if_no_pairs(self):
        dice_roll = [1, 2, 3, 4, 5, 6]
        category = "Pair"

        result = self.yahtzee.calculate_yahtzee_points(dice_roll, category)
        nt.assert_equal(result, 0)


    def test_should_return_1_when_pair_of_2(self):
        dice_roll = [1, 1, 1, 1, 1]
        category = "Pair"

        result = self.yahtzee.calculate_yahtzee_points(dice_roll, category)
        nt.assert_equal(result, 2)

    def test_should_return_8_when_pairs_of_1_and_3(self):
        dice_roll = [1, 1, 2, 3, 3]
        category = "Two Pairs"

        result = self.yahtzee.calculate_yahtzee_points(dice_roll, category)
        nt.assert_equal(result, 8)

    def test_should_return_0_when_one_pairs(self):
        dice_roll = [1, 1, 2, 4, 3]
        category = "Two Pairs"

        result = self.yahtzee.calculate_yahtzee_points(dice_roll, category)
        nt.assert_equal(result, 0)

    def test_should_return_4_when_five_ones(self):
        dice_roll = [1, 1, 1, 1, 1]
        category = "Two Pairs"

        result = self.yahtzee.calculate_yahtzee_points(dice_roll, category)
        nt.assert_equal(result, 4)