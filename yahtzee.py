class Yahtzee():
    @staticmethod
    def _count_occurrences(dice_roll):
        paired_value = []

        for i in range(6):
            paired_value += [i] * int(dice_roll.count(i) / 2)
        return paired_value

    @staticmethod
    def _calculate_highest_category_pair(paired_value):
        return max(paired_value) + max(paired_value)

    @staticmethod
    def _add_paired_values(paired_value):
        return paired_value[0] * 2 + paired_value[1] * 2

    @staticmethod
    def _calculate_total_score(category, count):
        return category * count

    def _get_score_by_category(self, category, dice_roll):
        count = 0

        for i in dice_roll:
            if i == category:
                count += 1
        return self._calculate_total_score(category, count)

    def _get_score_by_paired_value(self, dice_roll):
        paired_value = self._count_occurrences(dice_roll)

        if len(paired_value) == 0:
            return 0
        return self._calculate_highest_category_pair(paired_value)

    def _get_score_by_multiple_paired_values(self, dice_roll):
        paired_value = self._count_occurrences(dice_roll)

        if len(paired_value) != 2:
            return 0
        return self._add_paired_values(paired_value)

    def calculate_yahtzee_points(self, dice_roll, category):
        if type(category) == int:
            return self._get_score_by_category(category, dice_roll)
        elif category == "Pair":
            return self._get_score_by_paired_value(dice_roll)
        else:
            return self._get_score_by_multiple_paired_values(dice_roll)